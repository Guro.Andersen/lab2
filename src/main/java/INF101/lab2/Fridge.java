package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    int max_size = 20;
    List<FridgeItem> currentItemsInFridge = new ArrayList<FridgeItem>();

    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return currentItemsInFridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // Her må det plasseres en if slik at man har return for tomt kjøleskap og fullt kjøleskap
        if (currentItemsInFridge.size() >= max_size) {
            return false;
        }
            
        else {
            currentItemsInFridge.add(item);
            return true;
        }
            
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (!currentItemsInFridge.contains(item)) {
            throw new NoSuchElementException();
        }
        currentItemsInFridge.remove(item);
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        currentItemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List<FridgeItem> removedExpiredFoodItems = new ArrayList<>();
        for (FridgeItem item : currentItemsInFridge) {
            if (item.hasExpired()) {
                removedExpiredFoodItems.add(item);
            }
        }
        for (FridgeItem item : removedExpiredFoodItems) {
            currentItemsInFridge.remove(item);
        }
        return removedExpiredFoodItems;
    }
}
